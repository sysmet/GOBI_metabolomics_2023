# network functions
## Author: Patrick Dreher
## Update: 28.05.2020


#### Network basic functions ####

# format title tooltip for visNetwork
network_title<-function(.nodes){
  if(all(c("Metabolite","SUB.PATHWAY","SUPER.PATHWAY","HMDB","PUBCHEM","KEGG","Platform") %in% colnames(.nodes))){
    for(i in 1:nrow(.nodes)){
      if(.nodes$label[i] %in% .nodes$labels){
        .nodes$title[i] = paste(
          '<table>',
          '<tr>','<td>',"Metabolite:",'</td>','<td>',.nodes$Metabolite[i],'</td>','</tr>',
          '<tr>','<td>',"Super-pathway:",'</td>','<td>',.nodes$SUPER.PATHWAY[i],'</td>','</tr>',
          '<tr>','<td>',"Sub-pathway:",'</td>','<td>',.nodes$SUB.PATHWAY[i],'</td>','</tr>',
          '<tr>','<td>',"HMDB:",'</td>','<td>', ifelse(is.na(.nodes$HMDB[i]),"not available",paste('<a target="_blank" href="http://www.hmdb.ca/metabolites/',.nodes$HMDB[i],'/">',.nodes$HMDB[i],'</a>', sep="")),'</td>','</tr>',
          '<tr>','<td>',"PubChem:",'</td>','<td>', ifelse(is.na(.nodes$PUBCHEM[i]),"not available",paste('<a target="_blank" href="https://pubchem.ncbi.nlm.nih.gov/compound/',.nodes$PUBCHEM[i],'/">',.nodes$PUBCHEM[i],'</a>', sep="")),'</td>','</tr>',
          '<tr>','<td>',"KEGG:",'</td>','<td>',ifelse(is.na(.nodes$KEGG[i]),"not available",paste('<a target="_blank" href="http://www.genome.jp/dbget-bin/www_bget?cpd:',.nodes$KEGG[i],'">',.nodes$KEGG[i],'</a>', sep="")),'</td>','</tr>',
          '<tr>','<td>',"Platform:",'</td>','<td>',.nodes$Platform[i],'</td>','</tr>',
          '</table>'
        )
      }
      if(.nodes$label[i] %in% .nodes$SUPER.PATHWAY){
        .nodes$title[i] = paste(
          '<table>',
          '<tr>','<td>',"Super-pathway:",'</td>','<td>',.nodes$SUPER.PATHWAY[i],'</td>','</tr>',
          '<tr>','<td>',"Sub-pathway:",'</td>','<td>',"all",'</td>','</tr>',
          '</table>'
        )
      }
      if(.nodes$label[i] %in% .nodes$SUB.PATHWAY){
        .nodes$title[i] = paste(
          '<table>',
          '<tr>','<td>',"Super-pathway:",'</td>','<td>',.nodes$SUPER.PATHWAY[i],'</td>','</tr>',
          '<tr>','<td>',"Sub-pathway:",'</td>','<td>',.nodes$SUB.PATHWAY[i],'</td>','</tr>',
          '</table>'
        )
      }    
      if(.nodes$label[i] %in% c("metabolon plasma","metabolon urine","biocrates plasma")){
        .nodes$title[i] = NA
      }
    }
  }else{warning("annotate_network() could not overlap all necessary parameters")}
  return(.nodes)
}

# shape settings for visNetwork
network_shape<-function(.nodes){
  if(all(c("Metabolite","SUB.PATHWAY","SUPER.PATHWAY") %in% colnames(.nodes))){
    .nodes$shape="dot"
    for(i in which(.nodes$label %in% .nodes$SUB.PATHWAY)){
      .nodes$shape[i] = "triangle"
    }
    for(i in which(.nodes$label %in% .nodes$SUPER.PATHWAY)){
      .nodes$shape[i] = "ellipse"
    }
    for(i in which(.nodes$label %in% c("metabolon plasma","metabolon urine","biocrates plasma"))){
      .nodes$shape[i] = "box"
    }
    return(.nodes)
  }else{warning("network_shape() could not overlap all necessary parameters")}
}

# font settings for visNetwork
network_font<-function(.nodes){
  if(all(c("Metabolite","SUB.PATHWAY","SUPER.PATHWAY") %in% colnames(.nodes))){
    .nodes$font="30px arial black"
    for(i in which(.nodes$label %in% .nodes$SUB.PATHWAY)){
      .nodes$font[i] = "10px arial black"
    }
    for(i in which(.nodes$label %in% .nodes$SUPER.PATHWAY)){
      .nodes$font[i] = "20px arial black"
    }
    for(i in which(.nodes$label %in% c("metabolon plasma","metabolon urine","biocrates plasma"))){
      .nodes$font[i] = "30px arial black"
    }
  }else{warning("network_font() could not overlap all necessary parameters")}
  return(.nodes)
}

# size settings for visNetwork
network_size<-function(.nodes, size=30){
  .nodes$size=size
  return(.nodes)
}

# network resizing settings for visNetwork
network_resize<-function(.nodes){
  #strech network to x= -4 - 4 ; y= -3 - 3
  if(all(c("x","y") %in% colnames(.nodes))){
    rescale <- function(x) (x-min(x))/(max(x) - min(x))
    .nodes$x=(rescale(.nodes$x)*8)-4
    .nodes$y=(rescale(.nodes$y)*6)-3
  }else{warning("network_resize() could not overlap all necessary parameters")}
  return(.nodes)
}
