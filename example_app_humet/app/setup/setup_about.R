about=list()

about[["baseline_anthropometry"]] <- read.csv2(file="app/data/about/baseline_anthropometry.csv")
colnames(about[["baseline_anthropometry"]]) <- c("Parameter","Mean","Standard deviation", "Coefficient of variation")


about[["imputation_table"]] <- read.csv2(file="app/data/about/imputation_table.csv")
colnames(about[["imputation_table"]]) <- c("Fluid","Platform","Total number of metabolites", "Metabolites >= 30% missingness")
