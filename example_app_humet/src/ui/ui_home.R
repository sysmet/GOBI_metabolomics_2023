tabItem("home",
        fluidRow(style='background-image:URL("img/logo/logo_humet_runner.png");background-repeat:no-repeat;background-position: right;background-size:500px;text-align:center;min-height:200px;',
                 column(12,
                        h1(tags$span("The HuMet Repository",style='font-size:60px;font-weight:1000'),#tags$sup("beta",style='font-size:40px;font-weight:1000;color:black'),
                           style="text-align:center"),
                        h3("Time-resolved responses of the human metabolism", style="color:black")),
                 column(12,
                        div(style="max-width:100%; width:800px; margin-left:auto; margin-right:auto; display:flex",
                            div(class="home",
                                pickerInput(inputId="home_sel", label=NULL, choices=info_met$labels, selected = NULL, multiple = FALSE,width="100%",
                                            options = list(title = 'search metabolite',style = "mt_search-bar",'live-search'=TRUE)),style="flex:1"),
                            div(actionButton("home_divert", label=NULL,icon = rep_ui_icon("chart-area"),class="mt_search-submit")
                                
                            )
                        )
                 ),
                 column(12,
                        rep_header_stats(id= "header_info_metabolites",title="2652",subtitle="Metabolites"),
                        rep_header_stats(id= "header_info_platforms",title="9",subtitle="Platforms"),
                        rep_header_stats(id= "header_info_timepoints",title="56",subtitle="Time points"),
                        rep_header_stats(id= "header_info_challenges",title="6",subtitle="Challenges"),
                        rep_header_stats(id= "header_info_subjects",title="15",subtitle="Healthy subjects")
                 )
        ),
        
        rep_ui_vline(),
        fluidRow(
          h1("Showcases", style="text-align:center"),
          column(12,
                 rep_showcase_button(id="home_showcase_washout",
                                     title="Metabolites of prior exposure",
                                     subtitle="Putative dietary biormarkers indicating consumption of specific food ingredients",
                                     img_src="img/logo/logo_showcase_washout.png"),
                 rep_showcase_button(id="home_showcase_network_longi",
                                     title="Systemic metabolic responses",
                                     subtitle="Holistic overview of temporal responses to metabolic challenges",
                                     img_src="img/logo/logo_showcase_network.png"),
                 rep_showcase_button(id="home_showcase_platforms",
                                     title="Comparison of metabolic platforms",
                                     subtitle="Targeted MS (Biocrates p150) versus non-targeted MS (Metabolon HD4)",
                                     img_src="img/logo/logo_showcase_platform.png")
          )
        ),
        rep_ui_vline(),
        fluidRow(style="margin:0",
                 h1("Further reading", style="text-align:center"),
                 column(12,
                        p("Krug S, Kastenmüller G, Stückler F, Rist MJ, Skurk T, Sailer M, Raffler J, Römisch-Margl W, Adamski J, Prehn C, Frank T, Engel KH, Hofmann T, Luy B, Zimmermann R, Moritz F, Schmitt-Kopplin P, Krumsiek J, Kremer W, Huber F, Oeh U, Theis FJ, Szymczak W, Hauner H, Suhre K, Daniel H. ",
                          rep_href(href="https://www.ncbi.nlm.nih.gov/pubmed/22426117",label="The dynamic range of the human metabolome revealed by challenges.")," FASEB J. 2012 Jun;26(6):2607-19."),
                        style="padding:15px;")
        ),
        rep_ui_vline(),
        fluidRow(style="margin:20px 0 0 0;",
                 shinydashboardPlus::widgetUserBox(width = 3,background = T, closable = F,collapsible=F,
                                                   src = "img/home/icon_search.png",
                                                   backgroundUrl = "img/home/background_search.png",
                                                   column(12,h3("Metabolite selection",style="text-align:center")),
                                                   footer = "Find for your metabolite of interest. Use searchable tables to filter for platform, biofluid, pathway membership and implemented distance measures to find metabolites with similar kinetics."
                 ),
                 shinydashboardPlus::widgetUserBox(width = 3,background = T, closable = F,collapsible=F,
                                                   src = "img/home/icon_browser.png",
                                                   backgroundUrl = "img/home/background_browser.png",
                                                   column(12,h3("Temporal plots",style="text-align:center")),
                                                   footer = "Visualize your metabolic trajectories using mean or individual metabolite abundances within interactive plots. The visualization provides quick insight into metabolic kinetics over time."
                 ),
                 shinydashboardPlus::widgetUserBox(width = 3,background = T, closable = F,collapsible=F,
                                                   src = "img/home/icon_network.png",
                                                   backgroundUrl = "img/home/background_network.png",
                                                   column(12,h3("Networks",style="text-align:center")),
                                                   footer = "Get a holistic overview over all metabolites measured using the major targeted and non-targeted MS-based platforms in plasma and urine. Networks can be animated over time to reveal temporal metabolic changes."
                 ),
                 shinydashboardPlus::widgetUserBox(width = 3,background = T, closable = F,collapsible=F,
                                                   src = "img/home/icon_statistic.png",
                                                   backgroundUrl = "img/home/background_statistic.png",
                                                   column(12,h3("Statistical analysis",style="text-align:center")),
                                                   footer = "Perform statistical tests to find metabolites with significant changes during a challenge of interest or check the observed metabolomics variance by PCA analysis. All plots used and generated data can be downloaded."
                 )
        ),
        rep_ui_vline(),
        fluidRow(
          h2("The 'HuMet Repository' development team", style="text-align:center"),
          column(width=4,
                 boxProfile(
                   src = "img/about/developer_patrick.jpg",
                   subtitle = tags$span("PhD Student",br(),"Longitudinal metabolomics",style="color:black"),
                   title = tags$b(a(href="https://www.helmholtz-muenchen.de/ibis/research/metabolomics/staff/staff/ma/7218/-Dreher/index.html",target="_blank", font="black","Patrick Weinisch", style="color:black"))),
                 div(style="font-size:20px; text-align:center",
                     rep_social_button(url = "https://twitter.com/patrick__dreher",type = "twitter"),
                     rep_social_button(url = "https://www.linkedin.com/in/patrick-dreher-1b900a148/", type="linkedin"),
                     rep_social_button(url="patrick.dreher@helmholtz-muenchen.de",  type="mail"))
          ),
          column(width=4,
                 boxProfile(
                   src = "img/about/developer_johannes.jpg",subtitle = tags$span("Team Leader",br(),"Data Visualization and Integration Tools",style="color:black"),
                   title = tags$b(a(href="https://www.helmholtz-muenchen.de/ibis/research/metabolomics/staff/staff/ma/709/Dr.-Raffler/index.html",target="_blank", font="black",
                                    "Dr. Johannes Raffler", style="color:black"))),
                 div(style="font-size:20px; text-align:center",
                     rep_social_button(url = "https://twitter.com/JohannesRaffler",type = "twitter")
                 )
          ),
          column(width=4,
                 boxProfile(
                   src = "img/about/developer_gabi.jpg",subtitle = tags$span("Group Leader @ICB",br(),"Systems Metabolomics",style="color:black"),
                   title = tags$b(a(href="https://www.helmholtz-muenchen.de/ibis/research/metabolomics/staff/staff/ma/655/Dr.-Kastenm%C3%BCller/index.html",target="_blank", font="black",
                                    "Dr. Gabi Kastenmüller", style="color:black"))),
                 div(style="font-size:20px; text-align:center",
                     rep_social_button(url = "https://twitter.com/KastenmullerLab",type = "twitter"),
                     rep_social_button(url = "https://www.linkedin.com/in/gabi-kastenm%C3%BCller-8b4b5576/", type="linkedin"),
                     rep_social_button(url="g.kastenmueller@helmholtz-muenchen.de",  type="mail"))
          ),
          column(12,tags$b("Former developers:"), "Maria Schelling", style="padding-left:30px;")
          
        )
        
)