


# Normal repository box

rep_box<-function (..., id=NULL, title = NULL, footer = NULL, status = NULL, solidHeader = TRUE,
                  background = NULL, width = 6, height = NULL, collapsible = FALSE, maximizable=F, refresh=T,
                  collapsed = FALSE, closable = FALSE, enable_label = FALSE, title_color=NULL,download=NULL,
                  header_button=NULL, 
                  header_button_small=NULL,
                  label_text = NULL, label_status = "primary", enable_dropdown = FALSE, 
                  dropdown_icon = "gear", dropdown_menu = NULL, enable_sidebar = FALSE, 
                  sidebar_content = NULL, sidebar_width = 25, sidebar_background = "#ddd", sidebar_icon="table",
                  sidebar_start_open = FALSE, footer_padding = TRUE, footer_style=NULL, display_if=NULL) 
{
  if (sidebar_width < 0 || sidebar_width > 100) 
    stop("The box sidebar should be between 0 and 100")
  boxClass <- "rep-design box"
  if (solidHeader || !is.null(background)) {
    boxClass <- paste(boxClass, "box-solid")
  }
  if (!is.null(status)) {
    validateStatusPlus(status)
    boxClass <- paste0(boxClass, " box-", status)
  }
  if (collapsible && collapsed) {
    boxClass <- paste(boxClass, "collapsed-box")
  }
  if (!is.null(background)) {
    validateColor(background)
    boxClass <- paste0(boxClass, " bg-", background)
  }
  if (enable_sidebar) {
    if (sidebar_start_open) {
      boxClass <- paste0(boxClass, " direct-chat direct-chat-contacts-open")
    }
    else {
      boxClass <- paste0(boxClass, " direct-chat")
    }
  }
  style <- NULL
  if (!is.null(height)) {
    style <- paste0("height: ", shiny::validateCssUnit(height))
  }
  
  titleTagStyle<-NULL
  if(!is.null(title_color)){
    titleTagStyle=    paste0(titleTagStyle,"background-color:",title_color)
  }
  
  titleTag <- NULL
  if (!is.null(title)) {
    titleTag <- shiny::tags$h3(class = "box-title", title)
  }
  
  optionsBtnTag<-NULL
  if(!is.null(header_button)){
    optionsBtnTag=shiny::tags$div(header_button, class="rep_box-header_body")
  }
  
  headerBtnTag<-NULL
  if(!is.null(header_button_small)){
    headerBtnTag=shiny::tags$div(header_button_small, class="rep_box-header_body")
  }
  
  downloadBtnTag<-NULL
  if(!is.null(download)){
    downloadBtnTag= dropdownButton(circle=F,size="default",label="",icon=icon("download"),width="200px",right=T,
                                div(download, style="background:white; color:black; overflow:hidden;"))
    downloadBtnTag[["attribs"]][["style"]] = "float:left;height:45px;"
    downloadBtnTag[["children"]][[1]][["attribs"]][["class"]]=paste0("box-header_button ",downloadBtnTag[["children"]][[1]][["attribs"]][["class"]])
    downloadBtnTag[["children"]][[1]][["attribs"]][["style"]]="height:100%; color:white;"
  }
  
  boxToolTag <- NULL
  if (collapsible || closable) {
    boxToolTag <- shiny::tags$div(class = "box-tools pull-right")
  }
  collapseTag <- NULL
  if (collapsible) {
    buttonStatus <- "default"
    collapseIcon <- if (collapsed) 
      "plus"
    else "minus"
    collapseTag <- shiny::tags$button(class = paste0("box-header_button btn btn-box-tool"), 
                                      `data-widget` = "collapse", shiny::icon(collapseIcon))
  }
  closableTag <- NULL
  if (closable) {
    closableTag <- shiny::tags$button(class = "box-header_button btn btn-box-tool", 
                                      `data-widget` = "remove", type = "button", shiny::tags$i(shiny::icon("times")))
  }
  maximizableTag<- NULL
  if (maximizable) {
    maximizableTag<-actionButton(type = "button", class = "box-header_button btn btn-box-tool shiny-bound-input", onclick=paste0('maximizeBox("',id,'")'),
                                 inputId=paste0("max_",id),
                                 label=NULL,icon=icon("expand"),`data-widget` = "maximize")
  }
  refreshTag<- NULL
  if (refresh) {
    refreshTag<-actionButton(type = "button", class = "box-header_button btn btn-box-tool shiny-bound-input", inputId=paste0("refresh_",id),label=NULL,icon=icon("refresh"))
  }
  
  
  labelTag <- NULL
  if (enable_label) {
    labelTag <- dashboardLabel(label_text, status = label_status)
  }
  dropdownTag <- NULL
  if (enable_dropdown) {
    dropdownTag <- shiny::tags$div(class = "btn-group", 
                                   shiny::tags$button(type = "button", class = "btn btn-box-tool dropdown-toggle", 
                                                      `data-toggle` = "dropdown", shiny::icon(dropdown_icon)), 
                                   shiny::tagList(dropdown_menu))
  }
  
  sidebarTag <- NULL
  if (enable_sidebar) {
    sidebarTag <- shiny::tags$button(class = "btn btn-box-tool", 
                                     `data-widget` = "chat-pane-toggle", `data-toggle` = "tooltip", 
                                     `data-original-title` = "Plot data", title = NA, type = "button", 
                                     shiny::icon(sidebar_icon))
  }
  boxToolTag <- shiny::tagAppendChildren(optionsBtnTag,headerBtnTag, boxToolTag, labelTag, dropdownTag, sidebarTag,downloadBtnTag,refreshTag,maximizableTag, collapseTag, closableTag)
  headerTag <- NULL
  
  header_id <-NULL
  body_id<-NULL
  footer_id<-NULL
  
  if(!is.null(id)){
    header_id=paste0(id, "_header")
    body_id=paste0(id, "_body")
    footer_id=paste0(id, "_footer")
  }
  
  if (!is.null(titleTag) || !is.null(collapseTag)) {
    headerTag <- shiny::tags$div(class = "box-header", titleTag, style=titleTagStyle, id=header_id,
                                 tags$script(
                                   'function maximizeBox(id) {
                                   var element = document.getElementById(id);
                                   element.classList.toggle("maximize");
                                   
                                   this.classList.toggle("fa-thumbs-down");
  }
                                   
                                   '),
                                 boxToolTag)
  }
  
  boxPlusTag <- shiny::tags$div(class = if (!is.null(width))
    paste0("col-sm-", width), shiny::tags$div(class = boxClass, id=id,
                                              `data-display-if` = display_if,
                                              style = if (!is.null(style)) 
                                                style, headerTag, shiny::tags$div(class = "box-body", id=body_id,
                                                                                  ..., if (enable_sidebar) {
                                                                                    shiny::tags$div(style = "z-index: 10000;", class = "direct-chat-contacts", 
                                                                                                    shiny::tags$ul(class = "contacts-list", shiny::tags$li(style = paste0("width: ", 
                                                                                                                                                                          sidebar_width, "%;"), sidebar_content)))
                                                                                  }), if (!is.null(footer)) 
                                                                                    shiny::tags$div(style=footer_style, id=footer_id,class = if (isTRUE(footer_padding)) 
                                                                                      "box-footer"
                                                                                      else "box-footer no-padding", footer)))
  translation_rate <- paste0(100 - sidebar_width, "%")
  shiny::tagList(shiny::singleton(shiny::tags$head(shiny::tags$style(shiny::HTML(paste0(".direct-chat-contacts {\n                 -webkit-transform: translate(100%, 0);\n                 -ms-transform: translate(100%, 0);\n                 -o-transform: translate(100%, 0);\n                 transform: translate(100%, 0);\n                 position: absolute;\n                 top: 0;\n                 bottom: 0;\n                 height: 100%;\n                 width: 100%;\n                 background: ", 
                                                                                        sidebar_background, ";\n                 color: #fff;\n                 overflow: auto;\n              }\n              .direct-chat-contacts-open .direct-chat-contacts {\n                -webkit-transform: translate(", 
                                                                                        translation_rate, ", 0);\n                -ms-transform: translate(", 
                                                                                        translation_rate, ", 0);\n                -o-transform: translate(", 
                                                                                        translation_rate, ", 0);\n                transform: translate(", 
                                                                                        translation_rate, ", 0);\n              }\n              "))))), 
                 boxPlusTag)
}

# Dropdown list repository box
rep_box_right_item<-function(...,title=NULL,title_inline=T, max_width="100px"){
  titleTag=NULL
  if(!is.null(title)){
    titleTag=tags$span(title, style="font-weight:700;font-size:12px;margin:auto 0;")
  }
  div(class="rep_box-header_item",
      style=paste0(
        ifelse(title_inline,"display:flex;","display:block;"),
        "min-width:", validateCssUnit(max_width), ";"),
      titleTag,
      div(..., style="margin:auto 0")
  )
}


rep_mainsel_highlight<-function(text, ..., id){
  div(
    id=id,
    class="rep_mainsel_highlight",
    tags$b(text),
    ...
  )
}
